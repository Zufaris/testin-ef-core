﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testing_EF
{
    internal class Site
    {
        string name;
        string adress;
        int id;
        public static int count = 0;

        public string Name { get => name; set => name = value; }
        public string Adress { get => adress; set => adress = value; }
        public int Id { get => id; set => id = value; }

        public Site(string adress) 
        {
            this.name = adress;
            this.adress = adress;
            this.id = ++count;
        }

        public override string ToString()
        {
            return $"Site {Name} - {Adress}";
        }
    }
}
