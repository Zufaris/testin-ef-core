﻿namespace testing_EF
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello, World!");
            using (UserContext context = new UserContext())
            {
                bool exist = context.Database.EnsureCreated();
                if (!exist)
                {
                    var data = context.Users.Where(a => a.Stat!.Man == true).ToList();
                    User.count = data.Max(x => x.Id);

                    var dataSite = context.Sites.ToList();
                    Site.count = dataSite.Max(x => x.Id);
                    foreach (var user in data)
                    {
                        Console.WriteLine(user);
                    }

                    foreach (var site in dataSite)
                    {
                        Console.WriteLine(site);
                    }
                    //context.Database.EnsureDeleted();
                }
            }
            User mike = new User("Mike", "qwerty");
            User ben = new User("Ben", "ytrewq");
            Site hh = new Site("hh.ru");
            Site avito = new Site("avito.ru");
            using (UserContext db = new UserContext())
            {
                db.Add(mike);
                db.Add(ben);
                db.Add(avito); 
                db.Add(hh);
                db.SaveChanges();
            }
        }
    }
}