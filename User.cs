﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testing_EF
{
    internal class User
    {
        public static int count = 0;
        string name;
        string password;
        Statistics stat;

        public int Id { get; set; }
        public string Name { get => name; set => name = value; }
        public string Password { get => password; set => password = value; }
        internal Statistics Stat { get => stat; set => stat = value; }

        public User(string name, string password) 
        {
            this.name = name;
            this.password = password;
            Id = ++count;
            stat = new Statistics();
        }

        public override string ToString()
        {
            return $"User name: {name}, Password: {password}\t {stat}";
        }
    }
}
