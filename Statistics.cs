﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testing_EF
{
    internal class Statistics
    {
        int age;
        int studied;
        bool man;

        public int Age { get => age; set => age = value; }
        public int Studied { get => studied; set => studied = value; }
        public bool Man { get => man; set => man = value; }
        public Statistics() 
        {
            Random random = new Random();
            age = random.Next(18, 85);
            studied = random.Next(8, 20);
            man = random.Next(0, 99) % 2 == 0;
        }

        public override string ToString()
        {
            return $"Age: {age}; Years of study: {studied}; Man: {man}";
        }
    }
}
